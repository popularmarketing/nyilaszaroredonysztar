-- phpMyAdmin SQL Dump
-- version 4.4.7
-- http://www.phpmyadmin.net
--
-- Gép: localhost:3306
-- Létrehozás ideje: 2016. Már 31. 13:59
-- Kiszolgáló verziója: 5.6.26
-- PHP verzió: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Adatbázis: `alapwebsite`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `akciok`
--

CREATE TABLE IF NOT EXISTS `akciok` (
  `id` int(11) NOT NULL,
  `cim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` text COLLATE utf8_hungarian_ci NOT NULL,
  `image` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `akciok`
--

INSERT INTO `akciok` (`id`, `cim`, `leiras`, `image`) VALUES
(1, 'Kedvezmény', '<p>\r\n	Alum&iacute;nium redőnyre &eacute;s sz&uacute;nyogh&aacute;l&oacute;ra 10% kedvezm&eacute;ny</p>\r\n', '2e5ff-szunyoghalo.jpg'),
(2, 'Ingyen távirányító', '<p>\r\n	Aj&aacute;nd&eacute;k t&aacute;vir&aacute;ny&iacute;t&oacute; a motoros redőny&ouml;kh&ouml;z.</p>\r\n', '4d825-taviranyitos_motoros_redony.jpg'),
(3, 'Ingyenes felmérés', '<p>\r\n	Ingyenes felm&eacute;r&eacute;s, szaktan&aacute;csad&aacute;s, ingyenes &aacute;rkalkul&aacute;ci&oacute; &eacute;s helysz&iacute;ni felm&eacute;r&eacute;s. Forduljon hozz&aacute;nk bizalommal akkor is, ha &eacute;rdeklőd&eacute;se csak t&aacute;j&eacute;koz&oacute;d&aacute;s jellegű!</p>\r\n<p>\r\n	&nbsp;</p>\r\n', '49a71-two_people_talking.jpg');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `beallitasok`
--

CREATE TABLE IF NOT EXISTS `beallitasok` (
  `id` int(11) NOT NULL,
  `oldalnev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `logo` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `favicon` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `mobil` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `vezetekes` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `fax` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `adoszam` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `telephelycim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `uzletcim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyitvatartas` text COLLATE utf8_hungarian_ci NOT NULL,
  `adminemail` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyilvanosemail` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyitva` int(1) NOT NULL,
  `nyelv` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `fooldal_title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `fooldal_keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `fooldal_description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `google_analytics` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `beallitasok`
--

INSERT INTO `beallitasok` (`id`, `oldalnev`, `logo`, `favicon`, `mobil`, `vezetekes`, `fax`, `adoszam`, `telephelycim`, `uzletcim`, `nyitvatartas`, `adminemail`, `nyilvanosemail`, `nyitva`, `nyelv`, `fooldal_title`, `fooldal_keywords`, `fooldal_description`, `google_analytics`) VALUES
(1, 'Nyílászáró Redőny Sztár', '15cb2-logo.png', '', '+36/20-775-1576 ', '', '', '23329834-2-13', '', 'Kristály Brill-Shop KFT. 2360, Gyál .Budai Nagy Antal .u 6/1', '', '', 'nyilvanos@email.hu', 1, 'hu', '', '', '', ''),
(2, '', '', '', '', '', '', '', '', '', '', '', '', 0, 'de', '', '', '', ''),
(3, '', '', '', '', '', '', '', '', '', '', '', '', 0, 'en', '', '', '', ''),
(4, '', '', '', '', '', '', '', '', '', '', '', '', 0, 'cz', '', '', '', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `companytype`
--

CREATE TABLE IF NOT EXISTS `companytype` (
  `id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link1` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link2` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link3` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link4` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link5` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `companytype`
--

INSERT INTO `companytype` (`id`, `name`, `link1`, `link2`, `link3`, `link4`, `link5`) VALUES
(1, 'Étterem', '', '', '', '', ''),
(2, 'Fodrászat', '', '', '', '', ''),
(3, 'Cukrászda/Fagyizó', '', '', '', '', ''),
(4, 'Fitnesz / Gym / Edzőterem', '', '', '', '', ''),
(5, 'Tetóválószalon', '', '', '', '', ''),
(6, 'Plasztikai Sebészet', '', '', '', '', ''),
(7, 'Esküvői szalon', '', '', '', '', ''),
(8, 'Sexshop', '', '', '', '', ''),
(9, 'Ipari cikk', '', '', '', '', ''),
(10, 'Kozmetika', '', '', '', '', ''),
(11, 'Ruhazati boltok', '', '', '', '', ''),
(12, 'Utazasi iroda', '', '', '', '', ''),
(13, 'Konyveloi iroda', '', '', '', '', ''),
(14, 'Nyelviskola', '', '', '', '', ''),
(15, 'Fogászat', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `dokumentumok`
--

CREATE TABLE IF NOT EXISTS `dokumentumok` (
  `dokumentumokid` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `url` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `galeria`
--

CREATE TABLE IF NOT EXISTS `galeria` (
  `id` int(11) NOT NULL,
  `image_url` varchar(256) NOT NULL,
  `file` varchar(256) NOT NULL,
  `nev` varchar(256) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gyarto`
--

CREATE TABLE IF NOT EXISTS `gyarto` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gyik`
--

CREATE TABLE IF NOT EXISTS `gyik` (
  `cim` varchar(1000) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(255) NOT NULL,
  `tag` varchar(255) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `hirek`
--

CREATE TABLE IF NOT EXISTS `hirek` (
  `id` int(11) NOT NULL,
  `url` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `nev` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `kategoria` int(11) NOT NULL,
  `lead` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `datum` date NOT NULL,
  `statusz` int(11) NOT NULL,
  `kiemelt` int(11) NOT NULL,
  `fokep` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `tag` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `videoid` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `user` int(11) NOT NULL,
  `title` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` varchar(500) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `hirek_kategoria`
--

CREATE TABLE IF NOT EXISTS `hirek_kategoria` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) NOT NULL,
  `nyelv` varchar(5) NOT NULL,
  `fokep` varchar(256) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `kategoria`
--

CREATE TABLE IF NOT EXISTS `kategoria` (
  `kategoriaid` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `tipus` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `icon` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `markericon` varchar(128) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `kategoriak`
--

CREATE TABLE IF NOT EXISTS `kategoriak` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `szulo` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `kerdezzfelelek`
--

CREATE TABLE IF NOT EXISTS `kerdezzfelelek` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) CHARACTER SET utf32 COLLATE utf32_hungarian_ci NOT NULL,
  `email` varchar(256) NOT NULL,
  `kerdes` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_hungarian_ci NOT NULL,
  `datum` datetime NOT NULL,
  `kitol` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `marka`
--

CREATE TABLE IF NOT EXISTS `marka` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `megjelenes`
--

CREATE TABLE IF NOT EXISTS `megjelenes` (
  `megjelenesid` int(11) NOT NULL,
  `ugyfelszam` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `cegnev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `iranyitoszam` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `varos` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `cim` varchar(400) COLLATE utf8_hungarian_ci NOT NULL,
  `bevezeto` text COLLATE utf8_hungarian_ci NOT NULL,
  `kategoria` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `szoveg` text COLLATE utf8_hungarian_ci NOT NULL,
  `szoveg2` text COLLATE utf8_hungarian_ci NOT NULL,
  `youtubeid` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep1` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep2` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep3` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep4` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep5` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `telefonszam` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `email` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `mobil` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `webcim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyitvatartas` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `statusz` int(1) NOT NULL,
  `letrehozva` datetime NOT NULL,
  `lejarat` datetime NOT NULL,
  `gpsx` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `gpsy` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(128) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `nyelvi_forditasok`
--

CREATE TABLE IF NOT EXISTS `nyelvi_forditasok` (
  `id` int(11) NOT NULL,
  `azonosito` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `ertek` text COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` text COLLATE utf8_hungarian_ci NOT NULL,
  `file` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `oldalak`
--

CREATE TABLE IF NOT EXISTS `oldalak` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text COLLATE utf8_hungarian_ci NOT NULL,
  `szulo` int(11) NOT NULL,
  `cim` varchar(560) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `oldalak`
--

INSERT INTO `oldalak` (`id`, `nev`, `url`, `tartalom`, `szulo`, `cim`, `sorrend`, `statusz`, `fokep`, `title`, `keywords`, `description`, `header_custom_code`, `nyelv`) VALUES
(52, 'Főoldal', 'fooldal', '<h2>\r\n	Üdv a Nyílászáró Redőny Sztár Weboldalán</h2>\r\n<p>\r\nPélda szöveg, valami kéne "rólunk", csak semmi anyag nincs hozzá az eredeti weboldalon. Ez a szöveg változtatható a főoldal beállításainál.</p>\r\n<h3>\r\n	INGYENES HELYSZÍNI FELMÉRÉS</h3>\r\n', 0, '', 0, 1, '', '', '', '', '', ''),
(53, '404', 'nosite', '', 0, '', 0, 1, '', '', '', '', '', ''),
(54, 'Motoros redőny', 'motoros', '<div>\r\n	A motoros redőny nem csak k&eacute;nyelmes, de sz&aacute;mos előnye is van, valamint bizonyos m&eacute;ret felett, m&aacute;r elengedhetetlen. A motoros redőny automatiz&aacute;lhat&oacute;. Az is meghat&aacute;rozhat&oacute;, hogy milyen napszakban h&uacute;zza fel &eacute;s eressze le az adott redőnyt. &Iacute;gy abban az esetben ha elutazunk otthonr&oacute;l, &uacute;gy tűnik mintha otthon lenn&eacute;nk &eacute;s a bet&ouml;rők esz&eacute;n is t&uacute;lj&aacute;rhatunk. M&aacute;sik nagy előnye hogy egy gombnyom&aacute;sra az eg&eacute;sz h&aacute;z redőnye leereszthető. Amikor elmegy otthonr&oacute;l, csak egy nyom&aacute;s a t&aacute;vir&aacute;ny&iacute;t&oacute;n &eacute;s minden ny&iacute;l&aacute;sz&aacute;r&oacute; biztons&aacute;gba helyezhető.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Minden bet&ouml;r&eacute;s vagy lop&aacute;s eset&eacute;n a legfontosabb t&eacute;nyező az idő. Min&eacute;l tov&aacute;bb tart a bet&ouml;rőnek a lak&aacute;sba bejutni, ann&aacute;l nagyobb a lebuk&aacute;s es&eacute;lye, &iacute;gy a bűn&ouml;zők csak a k&ouml;nnyen hozz&aacute;f&eacute;rhető helyeket keresik. Egy kis befektet&eacute;ssel &Ouml;n is teljes biztons&aacute;gba tudhatja &eacute;rt&eacute;keit.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	A berendez&eacute;s komoly v&eacute;delmi automatik&aacute;val rendelkezik &iacute;gy azonnal meg&aacute;ll ha akad&aacute;lyt &eacute;rz&eacute;kel. &Iacute;gy elker&uuml;lhető, hogy a motor le&eacute;gjen, amikor a redőny esetleg egy akad&aacute;lyba &uuml;tk&ouml;zik.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	A motoros redőny vez&eacute;rl&eacute;se vezet&eacute;kes &eacute;s vezet&eacute;k n&eacute;lk&uuml;li megold&aacute;sban is kivitelezhető, vagy ak&aacute;r sz&aacute;m&iacute;t&oacute;g&eacute;p seg&iacute;ts&eacute;g&eacute;vel t&aacute;vvez&eacute;relt m&oacute;dszerrel is.</div>', 0, '', 0, 1, '5564e-taviranyitos_motoros_redony.jpg', '', '', '', '', ''),
(55, 'Műanyag redőny', 'muanyag', '<p>\r\n	A műanyag redőny a legn&eacute;pszerűbb redőnyfajta, mivel sz&aacute;mos előnye mellett igen kedvező &aacute;rakat tudunk biztos&iacute;tani. Igen k&ouml;nnyen felszerelhető &eacute;s haszn&aacute;lhat&oacute;, k&uuml;l&ouml;nleges karbantart&aacute;st nem ig&eacute;nyel. A legnagyobb bossz&uacute;s&aacute;g tal&aacute;n, ha műanyag redőnyre gondolunk, az az amikor az erős sz&eacute;l hat&aacute;s&aacute;ra, cs&ouml;r&ouml;g, z&ouml;r&ouml;g a szerkezet.</p>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Az &aacute;ltalunk felszerelt redőny&ouml;k v&eacute;gz&aacute;r&oacute;l redőnyl&eacute;ce gum&iacute;rozott &eacute;s rejtett &uuml;tk&ouml;zővel ell&aacute;tott, &iacute;gy t&ouml;k&eacute;letesen pormentes z&aacute;r&oacute;d&aacute;s biztos&iacute;t, valamint nem z&ouml;r&ouml;g m&eacute;g erős sz&eacute;lben sem.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	A redőny lamell&aacute;k UV &aacute;ll&oacute; tart&oacute;s műanyagb&oacute;l k&eacute;sz&uuml;ltek. Amennyiben &uacute;j lak&aacute;s &eacute;p&iacute;t&eacute;s vagy fel&uacute;j&iacute;t&aacute;s alkalm&aacute;val &nbsp;ker&uuml;l be&eacute;p&iacute;t&eacute;sre a műanyag redőny, abban az esetben k&eacute;rhető vakolhat&oacute; tokkal, &iacute;gy teljesen elrejthető a szerkezet.</div>\r\n', 0, '', 0, 1, '914e9-muanyag.jpg', '', '', '', '', ''),
(56, 'Alumínium redőny', 'aluminium', '<p>\r\n	Mi&eacute;rt is praktikus az alum&iacute;nium redőny? Legfontosabb &eacute;rvek k&ouml;z&eacute; tartozik, hogy minden alkatr&eacute;sze alum&iacute;niumb&oacute;l k&eacute;sz&uuml;l, kiv&aacute;l&oacute; hőszigetelő t&eacute;len, ny&aacute;ron &eacute;s a feltol&aacute;sg&aacute;tl&oacute;nak k&ouml;sz&ouml;nhetően, t&aacute;vol tartja az illet&eacute;ktelen behatol&oacute;kat is. Strapa&iacute;r&oacute;, tart&oacute;s, hossz&uacute; &eacute;lettartam&uacute;, kifejezetten tart&oacute;s m&eacute;g olyan helyeken is, ahol zordabb időj&aacute;r&aacute;si k&ouml;r&uuml;lm&eacute;nyek uralkodhatnak.</p>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	A redőnytok is alum&iacute;niumb&oacute;l &nbsp;k&eacute;sz&uuml;l, itt sincs panasz a minős&eacute;gre. Csap&aacute;gyazott horganytengellyel a redőny k&ouml;nnyen mozgathat&oacute;. Vakolhat&oacute; tokkal is k&eacute;rhető, ebben az esetben teljesen elrejthető, &iacute;gy m&eacute;g eszt&eacute;tikusabb.</div>\r\n', 0, '', 0, 1, '3e664-aluminium.jpg', '', '', '', '', ''),
(57, 'Vakolható redőny', 'vakolhato', '<p>\r\n	A vakolhat&oacute; redőnyrendszer alapvetően &uacute;jonnan &eacute;p&uuml;lt vagy fel&uacute;j&iacute;tand&oacute; h&aacute;zakba aj&aacute;nljuk. Nem csak eszt&eacute;tikus, modern kin&eacute;zete miatt, hanem kifejezetten hozz&aacute;seg&iacute;ti az &eacute;p&uuml;letet az energiatakar&eacute;kos fűt&eacute;shez is.</p>\r\n<div>\r\n	Alapvetően k&eacute;t megold&aacute;st &eacute;rdemes megfontolni. Az egyik a redőnygurtni kivezet&eacute;se az ablak felső r&eacute;sz&eacute;hez. Ekkora megfelelő helyet kell biztos&iacute;tani a szerelv&eacute;nynek.</div>\r\n<div>\r\n	M&aacute;sik megold&aacute;s amikor a kezel&eacute;snek egy k&uuml;l&ouml;n helyet hagyunk. Ekkora a szerv&iacute;zfed&eacute;len kereszt&uuml;l f&eacute;r&uuml;nk hozz&aacute; a szerkezethez.</div>\r\n<div>\r\n	&Iacute;gy az esetleges redőny karbantart&aacute;sa nem jelent probl&eacute;m&aacute;t &eacute;s ebben az esetben sincsen sz&uuml;ks&eacute;g a burkolat, vakolat megbont&aacute;s&aacute;ra.</div>\r\n<div>\r\n	Fontos előnyk&eacute;nt eml&iacute;thetj&uuml;k meg a legoptim&aacute;lisabb hő &eacute;s hangszigetel&eacute;st, &iacute;gy a lak&oacute;k a legnagyobb megel&eacute;ged&eacute;ssel haszn&aacute;lhatj&aacute;k redőnyeinket.</div>\r\n', 0, '', 0, 1, 'e68c2-vakolhato.jpg', '', '', '', '', ''),
(58, 'IGLO 5 műanyag', 'iglo', '<p>\r\n	OVLO 6 Műanyag ablak profil</p>\r\n<div>\r\n	Az Ovlo 6 kamr&aacute;s ablak 80 mm tok &eacute;s sz&aacute;rny sz&eacute;less&eacute;g&uuml; profilb&oacute;l k&eacute;sz&uuml;l.</div>\r\n<div>\r\n	Siegenia Aubi vasalattal &eacute;s alap esetbe is megkapja a fokozott h&ouml;szigetel&eacute;s&uuml; K=1.0 W/m2K &uuml;veget, de k&eacute;rhető bele K=07 W/m2K &uuml;veg is.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Az Ovlo term&eacute;kcsal&aacute;d Lengyelorsz&aacute;gba k&eacute;sz&uuml;l &eacute;s mind k&uuml;lem&eacute;be mind m&uuml;szaki tartalm&aacute;ba megeggyezik az Aluplast 7000 term&eacute;kcsal&aacute;ddal.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	- Ablak m&eacute;lys&eacute;g: 80mm</div>\r\n<div>\r\n	- Hő&aacute;tad&aacute;si t&eacute;nyező: 1,2 W/m2K</div>\r\n<div>\r\n	- Ablak profil: 6 l&eacute;gkamr&aacute;s ablakprofil UV- &eacute;s &uuml;t&eacute;s&aacute;ll&oacute; PVC (k&aacute;lcium- cink stabiliz&aacute;lt),&nbsp;</div>\r\n<div>\r\n	horganyzott ac&eacute;l merev&iacute;t&eacute;ssel.</div>\r\n<div>\r\n	- Ablak vasalat: N&eacute;met SIEGENIA-AUBI t&ouml;bb ponton z&aacute;r&oacute;d&oacute; biztons&aacute;gi vasalattal,</div>\r\n<div>\r\n	hib&aacute;sműk&ouml;dtet&eacute;s-g&aacute;tl&oacute;val, vasalati r&eacute;sszelőz&eacute;ssel.</div>\r\n<div>\r\n	- Ablak &uuml;vegez&eacute;s: Kiemelt hőszigetel&eacute;sű &uuml;veggel &ndash; k=1,0 W/m2K m&aacute;r alapkivitelben,</div>\r\n<div>\r\n	rendelhető 3 r&eacute;tegű &uuml;vegez&eacute;ssel vagy ak&aacute;r extra hangg&aacute;tl&oacute; &uuml;vegez&eacute;ssel.</div>\r\n<div>\r\n	- Bet&ouml;r&eacute;ssel szembeni ellen&aacute;ll&oacute;s&aacute;g:3-es bet&ouml;r&eacute;sbiztons&aacute;gi oszt&aacute;lyig.</div>\r\n<div>\r\n	- Hangg&aacute;tl&aacute;s:5-es hangg&aacute;tl&aacute;si oszt&aacute;lyig.</div>\r\n<div>\r\n	- Rendk&iacute;v&uuml;l rugalmas &eacute;s tart&oacute;s dupla EPDM t&ouml;m&iacute;t&eacute;sek biztos&iacute;tj&aacute;k azt,</div>\r\n<div>\r\n	hogy az ablakok l&eacute;gmentesen &eacute;s biztosan z&aacute;r&oacute;djanak.</div>\r\n<div>\r\n	- Kerek&iacute;tett design kialak&iacute;t&aacute;s.</div>\r\n<div>\r\n	- A rendk&iacute;v&uuml;l sima fel&uuml;letű keret megk&ouml;nny&iacute;ti a tiszt&iacute;t&aacute;st.</div>\r\n<div>\r\n	- 5 &eacute;v garancia!</div>\r\n', 0, '', 0, 1, '3714d-iglo.jpg', '', '', '', '', ''),
(59, 'OVLO 6', 'ovlo', '<p>\r\n	Az Ovlo 6 kamr&aacute;s ablak 80 mm tok &eacute;s sz&aacute;rny sz&eacute;less&eacute;g&uuml; profilb&oacute;l k&eacute;sz&uuml;l.<br />\r\n	Siegenia Aubi vasalattal &eacute;s alap esetbe is megkapja a fokozott h&ouml;szigetel&eacute;s&uuml; K=1.0 W/m2K &uuml;veget, de k&eacute;rhető bele K=07 W/m2K &uuml;veg is.<br />\r\n	Az Ovlo term&eacute;kcsal&aacute;d Lengyelorsz&aacute;gba k&eacute;sz&uuml;l &eacute;s mind k&uuml;lem&eacute;be mind m&uuml;szaki tartalm&aacute;ba megeggyezik az Aluplast 7000 term&eacute;kcsal&aacute;ddal.</p>\r\n<ul>\r\n	<li>\r\n		- Ablak m&eacute;lys&eacute;g: 80mm</li>\r\n	<li>\r\n		- Hő&aacute;tad&aacute;si t&eacute;nyező: 1,2 W/m2K</li>\r\n	<li>\r\n		- Ablak profil: 6 l&eacute;gkamr&aacute;s ablakprofil UV- &eacute;s &uuml;t&eacute;s&aacute;ll&oacute; PVC (k&aacute;lcium- cinkstabiliz&aacute;lt), horganyzott ac&eacute;l merev&iacute;t&eacute;ssel.</li>\r\n	<li>\r\n		- Ablak vasalat: N&eacute;met SIEGENIA-AUBI t&ouml;bb ponton z&aacute;r&oacute;d&oacute; biztons&aacute;gi vasalattal, hib&aacute;sműk&ouml;dtet&eacute;s-g&aacute;tl&oacute;val, vasalati r&eacute;sszelőz&eacute;ssel.</li>\r\n	<li>\r\n		- Ablak &uuml;vegez&eacute;s: Kiemelt hőszigetel&eacute;sű &uuml;veggel &ndash; k=1,0 W/m2K m&aacute;r alapkivitelben, rendelhető 3 r&eacute;tegű &uuml;vegez&eacute;ssel vagy ak&aacute;r extra hangg&aacute;tl&oacute; &uuml;vegez&eacute;ssel.</li>\r\n	<li>\r\n		- Bet&ouml;r&eacute;ssel szembeni ellen&aacute;ll&oacute;s&aacute;g:3-es bet&ouml;r&eacute;sbiztons&aacute;gi oszt&aacute;lyig.</li>\r\n	<li>\r\n		- Hangg&aacute;tl&aacute;s:5-es hangg&aacute;tl&aacute;si oszt&aacute;lyig.</li>\r\n	<li>\r\n		- Rendk&iacute;v&uuml;l rugalmas &eacute;s tart&oacute;s dupla EPDM t&ouml;m&iacute;t&eacute;sek biztos&iacute;tj&aacute;k azt, hogy az ablakok l&eacute;gmentesen &eacute;s biztosan z&aacute;r&oacute;djanak.</li>\r\n	<li>\r\n		- Kerek&iacute;tett design kialak&iacute;t&aacute;s.</li>\r\n	<li>\r\n		- A rendk&iacute;v&uuml;l sima fel&uuml;letű keret megk&ouml;nny&iacute;ti a tiszt&iacute;t&aacute;st.</li>\r\n	<li>\r\n		- 5 &eacute;v garancia!</li>\r\n</ul>\r\n', 0, '', 0, 1, '4abef-ovlo.jpg', '', '', '', '', ''),
(60, 'Árnyékoló - Reluxa', 'arnyekolo', '<p>\r\n	Reluxa kiv&aacute;l&oacute; belső &aacute;rny&eacute;kol&oacute; de lak&aacute;sunk d&iacute;szek&eacute;nt dekor&aacute;ci&oacute;jak&eacute;nt is szolg&aacute;l, nagy sz&iacute;nv&aacute;laszt&eacute;k&aacute;nak k&ouml;sz&ouml;nhetően.</p>\r\n<div>\r\n	A lamell&aacute;k d&ouml;nt&eacute;s&eacute;vel szab&aacute;lyozhatja a lak&aacute;sba vagy irod&aacute;ba bejut&oacute; f&eacute;ny mennyis&eacute;g&eacute;t. Kisebb dől&eacute;sű tetőt&eacute;ri ablakra is szerelhető.</div>\r\n<div>\r\n	Roletta: &Aacute;ra &ouml;sszetett, anyag&aacute;t sz&iacute;n&eacute;t kezel&eacute;s&eacute;t illetően, k&eacute;rje ingyenes &aacute;raj&aacute;nlatunkat!</div>\r\n<div>\r\n	Szerelhető oldalfalra, mennyezetre, k&ouml;zvetlen&uuml;l ablak- &eacute;s ajt&oacute;keretre.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Roletaa-barna Előnye, hogy ferde s&iacute;kokon is alkalmazhat&oacute;, &iacute;gy tetőt&eacute;ri ablakok &aacute;rny&eacute;kol&aacute;s&aacute;ra is kitűnő megold&aacute;s. A roletta helyig&eacute;nye feltekert &aacute;llapotban a v&aacute;szon hossz&aacute;t&oacute;l f&uuml;ggően 5-7 cm, ezt az ablak nyit&aacute;s&aacute;n&aacute;l figyelembe kell venni.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Nem csak szob&aacute;k, hanem irodahelys&eacute;gek &eacute;s t&eacute;likertek &aacute;rny&eacute;kol&oacute;ja is. Nagyobb ablakfel&uuml;letekn&eacute;l c&eacute;lszerű az oszt&aacute;snak megfelelően kisebb &aacute;rny&eacute;kol&oacute;k felszerel&eacute;se.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Műk&ouml;dtetet&eacute;si elv&eacute;t tekintve a roletta h&aacute;rom fő csoportba sorolhat&oacute;:</div>\r\n<div>\r\n	- gy&ouml;ngyl&aacute;ncos</div>\r\n<div>\r\n	- rug&oacute;s</div>\r\n<div>\r\n	- motoros</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Szalagf&uuml;gg&ouml;ny : &Aacute;ra &ouml;sszetett, anyag&aacute;t sz&iacute;n&eacute;t kezel&eacute;s&eacute;t illetően, k&eacute;rje ingyenes &aacute;raj&aacute;nlatunkat!</div>\r\n<div>\r\n	M&aacute;r nem csak k&ouml;zint&eacute;zm&eacute;nyek irod&aacute;k &aacute;rny&eacute;kol&oacute;ja a szalagf&uuml;gg&ouml;ny egyre elterjedtebb nappalikban h&aacute;l&oacute;szob&aacute;kban, egy&eacute;b helys&eacute;gekben,t&ouml;bb anyagminős&eacute;gből ,&eacute;s sz&iacute;nből v&aacute;laszthat ferde karnissal is szerelhető h&aacute;romsz&ouml;g ablak eset&eacute;n. A lamell&aacute;k forgat&aacute;s&aacute;val &aacute;ll&iacute;thatjuk a helys&eacute;gbe be&aacute;raml&oacute; f&eacute;ny mennyis&eacute;g&eacute;t.</div>\r\n', 0, '', 0, 1, '56a7c-arnyekolo.jpg', '', '', '', '', ''),
(61, 'Kapcsolat', 'kapcsolat', '', 0, '', 0, 1, '', '', '', '', '', ''),
(62, 'Árak', 'arak', '<p>\r\n	N&aacute;lunk tal&aacute;lja az orsz&aacute;g egyik legjobb nyil&aacute;sz&aacute;r&oacute; v&aacute;laszt&eacute;k&aacute;t. Amellett, hogy &aacute;ruink olcs&oacute;ak, a legjobb minős&eacute;gűek is orsz&aacute;gszerte.</p>\r\n', 0, '', 0, 1, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL,
  `sorrend` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `file` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `statusz` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `slider`
--

INSERT INTO `slider` (`id`, `sorrend`, `nev`, `file`, `nyelv`, `url`, `statusz`) VALUES
(1, 1, 'Redőnyök, Nyílászárók', '427ef-slider-1.png', '', 'Nálunk minden fajta redőny és nyílászáró megtalálható!', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `termekek`
--

CREATE TABLE IF NOT EXISTS `termekek` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kategoria` int(11) NOT NULL,
  `lead` text COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` text COLLATE utf8_hungarian_ci NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  `statusz` int(1) NOT NULL,
  `gyarto` int(11) NOT NULL,
  `marka` int(11) NOT NULL,
  `kiemelt` int(11) NOT NULL,
  `ar` int(11) NOT NULL,
  `ar_akcios` int(11) NOT NULL,
  `valuta` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `ar_beszerzesi` int(11) NOT NULL,
  `suly` int(11) NOT NULL,
  `raktarkeszlet` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `termekek`
--

INSERT INTO `termekek` (`id`, `nev`, `url`, `kategoria`, `lead`, `leiras`, `fokep`, `title`, `keywords`, `description`, `header_custom_code`, `nyelv`, `statusz`, `gyarto`, `marka`, `kiemelt`, `ar`, `ar_akcios`, `valuta`, `ar_beszerzesi`, `suly`, `raktarkeszlet`) VALUES
(1, 'Bukó-nyíló', '', 0, '', '<p>\r\n	90*150 buk&oacute;-ny&iacute;l&oacute;</p>\r\n', '', '', '', '', '', '', 0, 0, 0, 0, 43000, 0, '', 0, 0, 0),
(2, 'Bukó-nyíló', '', 0, '', '<p>\r\n	120*150 buk&oacute;-ny&iacute;l&oacute;</p>\r\n', '', '', '', '', '', '', 0, 0, 0, 0, 49000, 0, '', 0, 0, 0),
(3, 'Bukó-nyíló', '', 0, '', '<p>\r\n	150*150 buk&oacute;-ny&iacute;l&oacute;</p>\r\n', '', '', '', '', '', '', 0, 0, 0, 0, 55000, 0, '', 0, 0, 0),
(4, '2 szárnyú bukó-nyíló', '', 0, '', '<p>\r\n	150*150 2 sz&aacute;rny&uacute; buk&oacute;-ny&iacute;l&oacute;</p>\r\n', '', '', '', '', '', '', 0, 0, 0, 0, 70000, 0, '', 0, 0, 0),
(5, '2 szárnyú bukó-nyíló', '', 0, '', '<p>\r\n	210*150 2 sz&aacute;rny&uacute; buk&oacute;-ny&iacute;l&oacute;</p>\r\n', '', '', '', '', '', '', 0, 0, 0, 0, 92000, 0, '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `termek_kepek`
--

CREATE TABLE IF NOT EXISTS `termek_kepek` (
  `id` int(11) NOT NULL,
  `title` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `file` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `termek` int(11) NOT NULL,
  `sorrend` int(11) NOT NULL,
  `datum` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `termek_tulajdonsagok`
--

CREATE TABLE IF NOT EXISTS `termek_tulajdonsagok` (
  `id` int(11) NOT NULL,
  `termek` int(11) NOT NULL,
  `tulajdonsag_id` int(11) NOT NULL,
  `tulajdonsag` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `tulajdonsag`
--

CREATE TABLE IF NOT EXISTS `tulajdonsag` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `szulo` int(200) NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `tulajdonsag_kat`
--

CREATE TABLE IF NOT EXISTS `tulajdonsag_kat` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `perm` varchar(128) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `perm`) VALUES
(2, 'admin', 'c93ccd78b2076528346216b3b2f701e6', '1');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `akciok`
--
ALTER TABLE `akciok`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `beallitasok`
--
ALTER TABLE `beallitasok`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `companytype`
--
ALTER TABLE `companytype`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `dokumentumok`
--
ALTER TABLE `dokumentumok`
  ADD PRIMARY KEY (`dokumentumokid`);

--
-- A tábla indexei `galeria`
--
ALTER TABLE `galeria`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `gyarto`
--
ALTER TABLE `gyarto`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `gyik`
--
ALTER TABLE `gyik`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `hirek`
--
ALTER TABLE `hirek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `hirek_kategoria`
--
ALTER TABLE `hirek_kategoria`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `kategoria`
--
ALTER TABLE `kategoria`
  ADD PRIMARY KEY (`kategoriaid`);

--
-- A tábla indexei `kategoriak`
--
ALTER TABLE `kategoriak`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `kerdezzfelelek`
--
ALTER TABLE `kerdezzfelelek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `marka`
--
ALTER TABLE `marka`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `megjelenes`
--
ALTER TABLE `megjelenes`
  ADD PRIMARY KEY (`megjelenesid`);

--
-- A tábla indexei `nyelvi_forditasok`
--
ALTER TABLE `nyelvi_forditasok`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `oldalak`
--
ALTER TABLE `oldalak`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `termekek`
--
ALTER TABLE `termekek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `termek_kepek`
--
ALTER TABLE `termek_kepek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `termek_tulajdonsagok`
--
ALTER TABLE `termek_tulajdonsagok`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `tulajdonsag`
--
ALTER TABLE `tulajdonsag`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `tulajdonsag_kat`
--
ALTER TABLE `tulajdonsag_kat`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `akciok`
--
ALTER TABLE `akciok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT a táblához `beallitasok`
--
ALTER TABLE `beallitasok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT a táblához `companytype`
--
ALTER TABLE `companytype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT a táblához `dokumentumok`
--
ALTER TABLE `dokumentumok`
  MODIFY `dokumentumokid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `galeria`
--
ALTER TABLE `galeria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `gyarto`
--
ALTER TABLE `gyarto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `gyik`
--
ALTER TABLE `gyik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `hirek`
--
ALTER TABLE `hirek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `hirek_kategoria`
--
ALTER TABLE `hirek_kategoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT a táblához `kategoria`
--
ALTER TABLE `kategoria`
  MODIFY `kategoriaid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `kategoriak`
--
ALTER TABLE `kategoriak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `kerdezzfelelek`
--
ALTER TABLE `kerdezzfelelek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `marka`
--
ALTER TABLE `marka`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `megjelenes`
--
ALTER TABLE `megjelenes`
  MODIFY `megjelenesid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `nyelvi_forditasok`
--
ALTER TABLE `nyelvi_forditasok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT a táblához `oldalak`
--
ALTER TABLE `oldalak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT a táblához `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT a táblához `termekek`
--
ALTER TABLE `termekek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT a táblához `termek_kepek`
--
ALTER TABLE `termek_kepek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `termek_tulajdonsagok`
--
ALTER TABLE `termek_tulajdonsagok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `tulajdonsag`
--
ALTER TABLE `tulajdonsag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `tulajdonsag_kat`
--
ALTER TABLE `tulajdonsag_kat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
