<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Oldal extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");


        $this->load->model("Lekerdezes");
        $this->load->model("Alapfunction");
        $this->load->helper("url");
        $this->load->library("pagination");

    }

    public function index()
    {
			$data['beallitasok'] = $this->Lekerdezes->beallitasok();
			$data['oldal'] = $this->Lekerdezes->oldal('fooldal');
			$data['akciok'] = $this->db->query("SELECT * FROM akciok ORDER BY id DESC limit 0,4");
			$data['slider'] = $this->db->query("SELECT * FROM slider");
            $this->load->view("site/fooldal",$data);
    }

    public function url()
    {
		$url = $this->uri->segment(1);
		if(! file_exists(APPPATH."/views/site/".$url.".php")){
			$data['beallitasok'] = $this->Lekerdezes->beallitasok();
			$this->load->view("site/nosite", $data);
		}else{
			$data['beallitasok'] = $this->Lekerdezes->beallitasok();
			$data['oldal'] = $this->Lekerdezes->oldal($url);
			$data['akciok'] = $this->db->query("SELECT * FROM akciok ORDER BY id DESC limit 0,4");
			$data['termekek'] = $this->db->query("SELECT * FROM termekek");
			$this->load->view("site/".$url,$data);
			}
		}
 
    public function sendmail()
     {
         
            $this->load->helper('url');
            
            $nyelv = $this->input->get('lang', TRUE);
            $data['nyelv'] = $this->Alapfunction->nyelv($nyelv);
            $beallitasok = $this->Lekerdezes->beallitasok(" WHERE nyelv='".$data['nyelv']."' ");
            
           
            $senderName = $this->input->post("senderName");
            $email = $this->input->post("email");
			$email = $this->input->post("subject");
            $message = $this->input->post("message");          
          
            $this->load->library('email');
         
            $this->email->from($email ,$senderName);
            $this->email->to($beallitasok->nyilvanosemail); 

            $this->email->subject($subject);
            $this->email->message($message);    

            $this->email->send();
            
            redirect($uri='./');

         
     }
}

