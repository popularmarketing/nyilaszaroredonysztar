<?php include("header.php"); ?>
<div class="body_wrap">

<div class="header_container">
	
<div class="header_container">
<header>
        <div class="header_left">
		   	<div class="logo">
            <a href="<?php echo base_url();?>"><img src="<?php echo base_url("assets/uploads/files/".$sd['logo']);?>" alt=""></a>
            <h1>Segítek Venni!</h1>
            </div>
        </div>
        
        <div class="header_right">
        	<nav id="topmenu">            
	            <ul class="dropdown">
				<?php foreach($primari as $row){
					if(empty($row['gyerekek']) )
						echo '<li class="menu-item-home"><a href="'.base_url("oldal/url/".$row['url']).'"><span>'.$row['nev'].'</span></a></li>';
					else{
						echo '  <li><a href="'.base_url("oldal/url/".$row['url']).'"><span>'.$row['nev'].'</span></a><ul>';
						foreach($row['gyerekek'] as $gyRow)
                        	echo '<li><a href="'.base_url("oldal/url/".$gyRow['url']).'"><span>'.$gyRow['nev'].'</span></a></li>';	                    	
	                   echo '</ul></li>';
					}
				} ?>
	            </ul>                
			</nav>
        </div>
        <div class="clear"></div>			    
	</header>
</div>

<div class="container">

<div id="middle" class="cols2 sidebar_left">
	
    <div class="content" role="main">
    
    	<article class="post-item post-detail">
        	        
        	<h1><?php echo $oldal->nev?></h1>          
			
            
			<div class="entry">
              
            <?php echo $oldal->tartalom ?>
            	<div class="clear"></div>
            </div>
		</article>            
            
		</div>
           
    <div class="clear"></div>	    
</div>
<!--/ middle --> 
</div>
<!--/ container -->

</div>
<!--/ middle --> 
</div>
<!--/ container -->

<?php include("footer.php");?>

