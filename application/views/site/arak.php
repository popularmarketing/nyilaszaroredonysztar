<?php include('header.php');?>
<?php include('primari.php');?>
<!-- #page-title -->
	<section id="page-title">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<!-- .title -->
					<div class="title pull-left">
						<h1>pricing</h1>
					</div> <!-- /.title -->
					<!-- .page-breadcumb -->
					<div class="page-breadcumb pull-right">
						<i class="fa fa-home"></i> <a href="index.html">Home</a> <i class="fa fa-angle-right"></i> <span>pricing</span>
					</div> <!-- /.page-breadcumb -->
				</div>
			</div>
		</div>
	</section> <!-- /#page-title -->
	

	<!-- #pricing-content -->
	<section id="pricing-content">
		<div class="container">
			<div class="section-title">
				<h1>Árlistánk</h1>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<p><?php echo $oldal->tartalom;?></p>
				</div>
			</div>
			<div class="row price-table-wrap">
				<?php $count=1; foreach($termekek->result() as $row){
					if(($count-1)%4 == 0 || $count == 1){?>
						<div class="row">
					<?php }?>
						<div class="col-md-3 price-table bronze  zoomIn " data--duration=".5s" data--delay="0s">
							<div class="price-content">
								<div class="price-table-top">
									<h3><?php echo $row->nev?></h3>
								</div>
								<div class="price-box text-center">
									<span style="font-size: 180%"><?php echo $row->ar;?></span><p>Ft</p>
								</div>
								<p><?php echo $row->leiras?></p>
								<a href="kapcsolat">Lépjen velünk kapcsolatba</a>
							</div>
						</div>
				<?php if($count%4 == 0){?>
					</div>
					<div>
	&nbsp;</div>
					<?php } $count++; }?>
			</div>
		</div>
	</section><!-- /#pricing-content -->
<?php include('footer.php');?>