<?php include("header.php");?>
<?php include("primari.php");?>
<?php include("slider.php");?>
	<section id="our-services-mover">
		<div class="container">
			<div class="section-title">
				<h1>Aktuális akcióink</h1>
			</div>
			<div class="row">
				<?php foreach($akciok->result() as $row){?>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 thm-image-hover single-our-service-mover">
						<div class="img-holder">
							<img src="assets/uploads/files/<?php echo $row->image?>" alt="">
						</div>
						<a href="#"><h2 class="hvr-bounce-to-right"><?php echo $row->cim?></h2></a>
						<p><?php echo $row->leiras?></p>
					</div>
				<?php }?>
			</div>
		</div>
	</section>
	<section id="welcome-to-mover">
		<div class="container">
			<div class="row">
				<div class="col-lg-7 col-md-12">
					<?php print_r($oldal->tartalom);?>
					<div class="list-box clearfix">
						<ul>
							<li><i class="fa fa-hand-o-right"></i> Elérhető áron</li>
							<li><i class="fa fa-hand-o-right"></i> Minőségi munka</li>
						</ul>
						<ul>
							<li><i class="fa fa-hand-o-right"></i> Megbízható termékek</li>
							<li><i class="fa fa-hand-o-right"></i> Segítőkész dolgozók</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-5 col-md-5 thm-image-hover">
					<div class="img-holder">
						<img src="img/welcome-to-mover/1.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="our-process">
		<div class="container">
			<div class="section-title">
				<h1>A munkamenetünk</h1>
			</div>
			<div class="row">
				<div class="col-lg-12 clearfix">
					<div class="single-process">
						<div class="box">
							<i class="flaticon-2440"></i>
							<span>1</span>
						</div>
						<p>Megrendelés</p>
					</div>
					<div class="single-process">
						<div class="box">
							<i class="flaticon-packaging"></i>
							<span>2</span>
						</div>
						<p>Termékek bepakolása</p>
					</div>
					<div class="single-process">
						<div class="box">
							<i class="flaticon-trolley6"></i>
							<span>3</span>
						</div>
						<p>Termékek szállítása</p>
					</div>
					<div class="single-process">
						<div class="box">
							<i class="flaticon-stickman87"></i>
							<span>4</span>
						</div>
						<p>Termékek elhelyezése</p>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php include("footer.php");?>