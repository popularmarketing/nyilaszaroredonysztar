	<section id="great-moving-team-mover">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<h2><b>Megbízható</b> Cég</h2>
					<p class="emergency24hour">Akármilyen kérdésed van nyílászáró vagy redőny ügyben<br> hívjon minket bizalommal.</p>
					<p class="has-btn"><b><?php echo $beallitasok->mobil;?></b> vagy <a href="kapcsolat" class="hvr-bounce-to-right">Írjon nekünk</a></p>
				</div>
			</div>
		</div>
	</section>
	<!-- footer -->
	<footer class="mover">
		<div class="container">
			<div class="row">
				<!-- .widget -->
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 widget">
					<img class="positioned wow slideInLeft " src="img/resources/footer-man.png" alt="">
				</div> <!-- /.widget -->
				<!-- .widget -->
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 widget">
					<h3>Szolgáltatások</h3>
					<ul>
						<li>Redőny gyártás</li>
						<li>Redőny beszerelés, csere</li>
						<li>Szúnyogháló gyártás, beépítés</li>
						<li>Árnyékolók, reluxák forgalmazása, beépítése</li>
						<li>Gurtni csere, üzemképtelen redőnyök felújítása és javítása!</li>
						<li>Meglévő redőnyök motorizálása falikapcsolóval vagy távvezérlővel.</li>
						<li>Ablak, ajtó forgalmazása, beépítés</li>
					</ul>
					<ul class="social">
						<li><a href="#" class="hvr-radial-out"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#" class="hvr-radial-out"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#" class="hvr-radial-out"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="#" class="hvr-radial-out"><i class="fa fa-linkedin"></i></a></li>
					</ul>
				</div> <!-- /.widget -->
				<!-- .widget -->
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 widget clearboth-tab">
					<h3>Termékeink</h3>
					<ul class="popular-post">
						<li>
							<a href="motoros"><h5><i class="fa fa-angle-right angle-right-icon"></i>Motoros Redőny</h5></a>
						</li>
						<li>
							<a href="muanyag"><h5><i class="fa fa-angle-right angle-right-icon"></i>Műanyag Redőny</h5></a>
						</li>
						<li>
							<a href="aluminium"><h5><i class="fa fa-angle-right angle-right-icon"></i>Alumínium Redőny</h5></a>
						</li>
                        <li>
							<a href="vakolhato"><h5><i class="fa fa-angle-right angle-right-icon"></i>Vakolható Redőny</h5></a>
						</li>
                        <li>
							<a href="iglo"><h5><i class="fa fa-angle-right angle-right-icon"></i>IGLO 5 Műanyag</h5></a>
						</li>
						<li>
							<a href="ovlo"><h5><i class="fa fa-angle-right angle-right-icon"></i>OVLO 6</h5></a>
						</li>
						<li>
							<a href="arnyekolo"><h5><i class="fa fa-angle-right angle-right-icon"></i>Árnyékoló - Reluxa</h5></a>
						</li>
					</ul>
				</div> <!-- /.widget -->
				<!-- .widget -->
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 widget">
					<h3>Lépjen velünk kapcsolatba</h3>
					<ul class="contact-info">
						<li><i class="fa fa-map-marker"></i> <?php echo $beallitasok->uzletcim;?></li>
						<li><i class="fa fa-phone"></i> <?php echo $beallitasok->mobil;?></li>
						<li><i class="fa fa-envelope-o"></i> <?php echo $beallitasok->nyilvanosemail;?></li>
						<li><i class="fa fa-globe"></i> <?php echo base_url();?></li>
					</ul>
				</div> <!-- /.widget -->
				
			</div>
		</div>
	</footer> <!-- /footer -->
	
	<!-- #bottom-bar -->
	<section id="bottom-bar" class="mover">
		<div class="container">
			<div class="row">
				<!-- .copyright -->
				<div class="copyright pull-left">
					<p>Copyright &copy; 2016. Minden jog fenntartva. </p>
				</div> <!-- /.copyright -->
				<!-- .credit -->
				<div class="pull-right row">
					<a href="http://www.popularmarketing.hu" target="_blank">
					<div class="credit col-sm-7">
						<p class="pull-right">Weboldal készítés</p>
					</div>
					<div class="col-sm-5">
						<img alt="popularmarketing.hu" src="img/logo.png" style="margin-top:15px;">
					</div>
					</a>
				</div>
			</div>
		</div> 
	</section><!-- /#bottom-bar -->


	
	<script src="js/jquery.min.js"></script> <!-- jQuery JS -->
	<script src="js/bootstrap.min.js"></script> <!-- BootStrap JS -->
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script> <!-- Gmap Helper -->
	<script src="js/gmap.js"></script> <!-- gmap JS -->
	<script src="js/wow.js"></script> <!-- wow JS -->
	<script src="js/isotope.pkgd.min.js"></script> <!-- iSotope JS -->
	<script src="js/owl.carousel.min.js"></script> <!-- OWL Carousle JS -->
	
	<script src="revolution/js/jquery.themepunch.tools.min.js"></script> <!-- Revolution Slider Tools -->
	<script src="revolution/js/jquery.themepunch.revolution.min.js"></script> <!-- Revolution Slider -->
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.actions.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.carousel.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.migration.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.navigation.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.parallax.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.video.min.js"></script>

	<script src="js/jquery.fancybox.pack.js"></script> <!-- FancyBox -->
	<script src="js/validate.js"></script> <!-- Form Validator JS -->
	<script src="js/jquery.easing.min.js"></script> <!-- jquery easing JS -->
	<script src="js/custom.js"></script> <!-- Custom JS -->

</body>
</html>