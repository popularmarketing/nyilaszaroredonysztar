<?php include("header.php");?>
<?php include("primari.php");?>

	<!-- #page-title -->
	<section id="page-title">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<!-- .title -->
					<div class="title pull-left">
						<h1>Kapcsolat</h1>
					</div> <!-- /.title -->
					<!-- .page-breadcumb -->
					<div class="page-breadcumb pull-right">
						<i class="fa fa-home"></i> <a href="index.html">Főoldal</a> <i class="fa fa-angle-right"></i> <span>Kapcsolat</span>
					</div> <!-- /.page-breadcumb -->
				</div>
			</div>
		</div>
	</section> <!-- /#page-title -->
	
	<!-- #contact-content -->
	<section id="contact-content">
		<div class="container">
			<div class="row">
				<p>Írjon nekünk bármikor üzenetet, vagy hívjon minket telefonon: <?php echo $beallitasok->mobil;?></p>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
					<form action="<?php echo base_url("oldal/sendmail");?>" method="post" class="contact-form">
						<p><input type="text" name="senderName" placeholder="Név"></p>
						<p><input type="text" name="email" placeholder="Email"></p>
						<p><input type="text" name="subject" placeholder="Tárgy"></p>
						<p><textarea name="message" placeholder="Üzenet"></textarea></p>
						<p><button type="submit">Elküldés</button></p>
					</form>
				</div>
                <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12  col-lg-offset-1  col-md-offset-0  col-sm-offset-0  col-xs-offset-0">
                	<div class="contact-right-img">
                    	<img src="img/contact-page/contact-img.jpg" alt="">
                    </div>
                </div>
			</div>
		</div>
	</section> <!-- /#contact-content -->
    
    <section id="contact-info-area">
    	<div class="container">
        	<div class="row">
            	<div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 contact-info-part">
					<h3>Lépjen velünk kapcsolatba!</h3>
					<p>Van kérdése? Adja a tudtunkra.</p>
					<ul>
						<li class="clearfix">
                        	<div class="contact-icon">
								<img src="img/contact-page/icon/1.png" alt=""> 
                            </div>
							<div class="content">
								<h4>Üzlet</h4>
								<p><?php echo $beallitasok->uzletcim?></p>
							</div>
						</li>
						<li class="clearfix">
                        	<div class="contact-icon">
								<img src="img/contact-page/icon/2.png" alt=""> 
                            </div>
							<div class="content">
								<h4>Email</h4>
								<p><?php echo $beallitasok->nyilvanosemail?></p>
							</div>
						</li>
						<li class="clearfix">
                        	<div class="contact-icon">
								<img src="img/contact-page/icon/3.png" alt="">
                            </div> 
							<div class="content">
								<h4>Telefon</h4>
								<p><?php echo $beallitasok->mobil?></p>
							</div>
						</li>
					</ul>
				</div>
                <div class="col-lg-7 col-md-6 col-sm-12 col-xs-12">
                	<div class="google-map" id="contact-google-map" data-map-lat="47.38573" data-map-lng="19.23831" data-icon-path="img/resources/map-marker.png" data-map-title="Üzletünk" data-map-zoom="12"></div>
                </div>
            </div>
        </div>
    </section>
<?php include("footer.php");?>