<?php include("header.php");?>
<?php include("primari.php");?>
	<!-- #page-title -->
	<section id="page-title">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<!-- .title -->
					<div class="title pull-left">
						<h1>404 Hiba</h1>
					</div> <!-- /.title -->
					<!-- .page-breadcumb -->
					<div class="page-breadcumb pull-right">
						<i class="fa fa-home"></i> <a href="index.html">Főoldal</a> <i class="fa fa-angle-right"></i> <span>404</span>
					</div> <!-- /.page-breadcumb -->
				</div>
			</div>
		</div>
	</section> <!-- /#page-title -->
	
	
	<section id="page-404-content">
		<div class="container">
			<div class="row">
				<img src="img/404/404-man.png" alt="" class="man-404">
				<div class="col-lg-7 col-md-7 col-sm-9 col-xs-12 col-lg-offset-5 col-md-offset-5 col-sm-offset-2">
					<h1>404: Oldal nem találva</h1>
					<p>Mindenhol megnéztük, de sajnos a keresett oldalt nem találjuk.</p>
					<a href="<?php echo base_url();?>" class="button-404">Vissza a főoldalra</a>
				</div>
			</div>
		</div>
	</section>
<?php include("footer.php");?>