	<!-- #topbar -->
	<section id="topbar" class="mover">
		<div class="container">
			<div class="row">
				<div class="contact-info pull-left">
					<ul>
                <li><a href="kapcsolat"><i class="fa fa-map-marker"></i><?php echo $beallitasok->uzletcim?></a></li>
                <li><a href="kapcsolat"><i class="fa fa-envelope"></i><?php echo $beallitasok->nyilvanosemail?></a></li>
					</ul>
				</div> <!-- /.contact-info -->
                <div class="phone-number">
                	<p><span>Hívjon minket bizalommal:</span> <?php echo $beallitasok->mobil?></p>
                </div>
			</div>
		</div>
	</section> <!-- /#topbar -->

	<!-- header -->
	<header class="mover">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-4 col-lg-offset-0 col-md-offset-4 logo">
					<a href="<?php echo base_url();?>">
						<img src="assets/uploads/files/<?php echo $beallitasok->logo;?>" alt="Plumberx">
					</a>
				</div>
				<nav class="col-lg-9 col-md-12 col-lg-pull-0 col-md-pull-1 mainmenu-container">
					<button class="mainmenu-toggler">
						<i class="fa fa-bars"></i>
					</button>
					<ul class="mainmenu pull-right">
						<li class="dropdown">
							<a>Redőny</a>
							<ul class="submenu">
								<li><a href="motoros">Motoros Redőny</a></li>
								<li><a href="muanyag">Műanyag Redőny</a></li>
								<li><a href="aluminium">Alumínium Redőny</a></li>
								<li><a href="vakolhato">Vakolható Redőny</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a>Nyílászárók</a>
							<ul class="submenu">
								<li><a href="iglo">IGLO 5 műanyag</a></li>
								<li><a href="ovlo">OVLO 6</a></li>
								<li><a href="arak">Árak</a></li>
							</ul>
						</li>
						<li><a href="arnyekolo">Árnyékoló - Reluxa</a></li>
						<li><a href="kapcsolat">Kapcsolat</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</header> <!-- /header -->