<section class="rev_slider_wrapper mover-banner">
		<div id="slider1" class="rev_slider"  data-version="5.0">
			<ul>
				<?php foreach($slider->result() as $row){?>
					<li data-transition="parallaxvertical">
						<img src="assets/uploads/slider/<?php echo $row->file?>"  alt="" width="1920" height="705" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="<?php echo $row->sorrend?>" >
						<div class="tp-caption sfr tp-resizeme gardener-caption-h1 bg-white font-light" 
							data-x="left" data-hoffset="0" 
							data-y="top" data-voffset="251" 
							data-whitespace="nowrap"
							data-transform_idle="o:1;" 
							data-transform_in="o:0" 
							data-transform_out="o:0" 
							data-start="1200">
							<span class="bold"><?php echo $row->nev?></span>
						</div>
						<div class="tp-caption sfb tp-resizeme gardener-caption-p" 
							data-x="left" data-hoffset="0" 
							data-y="top" data-voffset="340" 
							data-whitespace="nowrap"
							data-transform_idle="o:1;" 
							data-transform_in="o:0" 
							data-transform_out="o:0" 
							data-start="1600">
							<?php echo $row->url?>
						</div>
					</li>				
				<?php }?>
			</ul>
		</div>
	</section>