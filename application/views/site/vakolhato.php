<?php include("header.php");?>
<?php include("primari.php");?>
	<section id="page-title">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<!-- .title -->
					<div class="title pull-left">
						<h1><?php echo $oldal->nev;?></h1>
					</div> <!-- /.title -->
					<!-- .page-breadcumb -->
					<div class="page-breadcumb pull-right">
						<i class="fa fa-home"></i> <a href="index.html">Főoldal</a> <i class="fa fa-angle-right"></i> <span><?php echo $oldal->nev;?></span>
					</div> <!-- /.page-breadcumb -->
				</div>
			</div>
		</div>
	</section> <!-- /#page-title -->
	<section>
		<div class="container" style="margin-top: 65px; margin-bottom:65px;">
			<div class="row">
				<div class="col-sm-7">
					<?php print_r($oldal->tartalom);?>
				</div>
				<div class="col-sm-1"></div>
				<div class="col-sm-4">
					<img alt="//" src="assets/uploads/files/<?php echo $oldal->fokep?>" class="pull-right">
				</div>
			</div>
		</div>
	</section>
<?php include("footer.php");?>